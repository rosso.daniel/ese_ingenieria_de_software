#include "gpio.h"
#include <Arduino.h>

unsigned long int gpio_counter(int pin) 
{
  unsigned long int counter = 0;
  unsigned int buttonState = 0;
  unsigned int buttonState_ff = 0;

  buttonState = digitalRead(pin);
  delay(10);
  buttonState_ff = digitalRead(pin);

  counter = 0;
  while ((buttonState == buttonState_ff) && (buttonState_ff == HIGH)) {
    counter += 1;
    buttonState = digitalRead(pin);
    delay(10);
    buttonState_ff = digitalRead(pin);
  }

  return counter;
}



void toggle_gpio(int pin) 
{
  digitalWrite(pin, !digitalRead(pin)); // Toggle the LED state
  delay(100);

}
