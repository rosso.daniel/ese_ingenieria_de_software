#ifndef GPIO_H
#define GPIO_H

unsigned long int gpio_counter(int pin);
void               toggle_gpio(int pin); 

#endif
