#include "adc.h"
#include <Arduino.h>

#define BITS 1023.0
#define VCC 5

float sense_analog(int adcPinLocal)
{
  int adc_counter;
  float adcValue;
  adc_counter = analogRead(adcPinLocal);
  adcValue = (float)adc_counter * (VCC/BITS);
  return(adcValue);
}