#include "gpio.h"
#include "adc.h"

/**
 * @brief Pin definitions
 */
const int keepAlive = 13;       ///< Keep-alive pin
const int alarmedLedPin = 12;   ///< LED pin indicating alarm status
const int buzzerPin = 4;        ///< Buzzer pin for alarm sound
const int buttonPin = 8;        ///< Button pin for control
const int on_off_Sensor = 9;    ///< On/Off sensor pin

const int adcPin = A0;          ///< Analog sensor pin

/**
 * @brief Enumeration representing different states of the system
 */
enum State 
{
  CALIBRATION,  ///< Calibration state
  SENSING,      ///< Sensing state
  ALARMED,      ///< Alarm state
  DEBUG         ///< Debug state
};

State currentState = CALIBRATION;  ///< Current state of the system

/**
 * @brief Setup function, initializes pins and serial communication
 */
void setup() 
{
  pinMode(keepAlive, OUTPUT);
  pinMode(alarmedLedPin, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  pinMode(on_off_Sensor, INPUT);
  pinMode(buttonPin, INPUT);
  Serial.begin(115200);
}

/**
 * @brief Main loop function
 */
void loop() 
{
  // Function to toggle keepAlive pin
  toggle_gpio(keepAlive);
  
  // Counter variables
  static unsigned long int on_off_sensor_counter = 0;
  static unsigned long int pressed_counter;
  static float threshold;
  static float analog_sensor;
  
  // State machine
  switch (currentState) 
  {
    case CALIBRATION:
      toggle_gpio(keepAlive);
      Serial.println("System in Calibration");
      threshold = receiveFloatFromSerial();  
      currentState = SENSING;
      break;

    case SENSING:
      toggle_gpio(keepAlive);
      Serial.println("Sensing");
      analog_sensor = sense_analog(adcPin);
      Serial.println(analog_sensor);
      
      on_off_sensor_counter = gpio_counter(on_off_Sensor);
      if (on_off_sensor_counter > 100) {
        on_off_sensor_counter = 0;
        currentState = ALARMED;
      }
      if (analog_sensor > threshold) {
        currentState = ALARMED;
      }
      toggle_gpio(keepAlive);
      break;

    case ALARMED:
      toggle_gpio(keepAlive);
      Serial.println("Alarmed");
      toggle_gpio(alarmedLedPin);

      pressed_counter = gpio_counter(buttonPin);
      Serial.println(pressed_counter);
      if (pressed_counter > 100) 
      {
        currentState = DEBUG;
        digitalWrite(alarmedLedPin, LOW); // Toggle the LED state
      } else {
        if (pressed_counter > 0 && pressed_counter < 100) {
          currentState = SENSING;
            digitalWrite(alarmedLedPin, LOW); // Toggle the LED state
        } else {
          currentState = ALARMED;
        }
      }
      break;

    case DEBUG:
      toggle_gpio(keepAlive);
      Serial.println("Debug state activated");
      currentState = CALIBRATION;
      break;
  }
  delay(500);
  toggle_gpio(keepAlive);
}

/**
 * @brief Function to receive a floating-point number from Serial
 * @return The received floating-point number
 */
float receiveFloatFromSerial() {
  String inputString = ""; // Buffer to store received characters
  char c;
  int calibrated_flag = 0;
  float threshold;
  while (calibrated_flag == 0) {
    while (Serial.available()) {
      c = Serial.read();
      if (c == '\n' || c == '\r') {        
        threshold = inputString.toFloat(); // Convert the received string to a float
        Serial.println("Threshold entered");
        Serial.println(threshold);
        calibrated_flag = 1;
      } else {
        inputString += c; // Add the character to the input string
      }
    }
  }
  return(threshold);
}


