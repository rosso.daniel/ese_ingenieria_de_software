#include <stdio.h>
#include <stdint.h>



int main() 
{
    float numeroFlotante = 3.141516; // numero de punto flotante
    int16_t entero16Bits;
   

    //Verificación de overflow
    if (numeroFlotante > INT16_MAX) 
    {
        entero16Bits = INT16_MAX;
    }
    //Verificación de underflow
    else if (numeroFlotante < INT16_MIN) 
    {
        entero16Bits = INT16_MIN;
    }
    else
    {
        //Conversión de un entero válido.
        entero16Bits =  (int16_t) numeroFlotante;
    }

    printf("Número de punto flotante: %f\n", numeroFlotante);
    printf("Entero de 16 bits: %d\n", entero16Bits);

    return 0;
}
